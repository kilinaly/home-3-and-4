﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FingerDriverTrack : MonoBehaviour
{
    public class TrackSegment
    {
        public Vector3[] Points = new Vector3[3];
        public bool IsPointInSegment(Vector3 point)
        {
            return MathfTriangles.IsPointInTriangleXY(point,
                Points[0], Points[1], Points[2]);
        }
    }

    [SerializeField] private LineRenderer m_LineRenderer;
    [SerializeField] private bool m_ViewDebug;

    private Vector3[] corners;
    private TrackSegment[] segments;
    
    // Start is called before the first frame update
    void Start()
    {
        //Заполняем массив опорных точек трассы
        corners = new Vector3[transform.childCount];
        for (int i = 0; i < corners.Length; i++)
        {
            GameObject obj = transform.GetChild(i).gameObject;
            corners[i] = obj.transform.position;
            obj.GetComponent<MeshRenderer>().enabled = false;
        }
        
        //настраиваем LineRenderer
        m_LineRenderer.positionCount = corners.Length;
        m_LineRenderer.SetPositions(corners);
        
        //из полученного LineRenderer запекаем mesh
        Mesh mesh = new Mesh();
        m_LineRenderer.BakeMesh(mesh, true);
        
        //создаем массив сегментов трассы
        //каждый треугольник описан 3-мя вершинами из массива вершин
        segments = new TrackSegment[mesh.triangles.Length / 3];
        int segmentsCounter = 0;
        for (int i = 0; i < mesh.triangles.Length; i+=3)
        {
            segments[segmentsCounter] = new TrackSegment();
            segments[segmentsCounter].Points = new Vector3[3];
            int nm = mesh.triangles[i];
            segments[segmentsCounter].Points[0] = mesh.vertices[nm];
            nm = mesh.triangles[i + 1];
            segments[segmentsCounter].Points[1] = mesh.vertices[nm];
            nm = mesh.triangles[i + 2];
            segments[segmentsCounter].Points[2] = mesh.vertices[nm];
            segmentsCounter++;
        }

        if (!m_ViewDebug)
        {
            return;
        }

        foreach (var segment in segments)
        {
            foreach (var point in segment.Points)
            {
                GameObject sphere = 
                    GameObject.CreatePrimitive(PrimitiveType.Sphere);

                sphere.transform.position = point;
                sphere.transform.localScale = Vector3.one * 0.1f;
            }
        }
    }

    /// <summary>
    /// Определяем находится ли точка в пределах трассы
    /// </summary>
    /// <param name="point">точка</param>
    /// <returns></returns>
    public bool IsPointInTrack(Vector3 point, int  previousCheckpoint)
    {
        int previousCheckpoint3 = previousCheckpoint * 3;//сегмент предыдущего чекпоинта
        int nextCheckpoint3 = (previousCheckpoint+1) * 3;//сегмент следующего чекпоинта

        int i = 0;
        foreach (var segment in segments)
        {
            if((i>=previousCheckpoint3)&&(i<nextCheckpoint3))
            {
            if (segment.IsPointInSegment(point))
            {
                return true;
            }
            }
            i++;
        }

        return false;
    }

    

    public int PreviousCheckPoint(Vector3 point, int previousCheckpoint)//возвращает номер пройденного до этого чекпоинта
    {
        int i = 0;
        int j = 0;
        int blockNumber = 0;//номер блока с сегментами(3) или предыдущий уже пройденный чекпоинт
     
        foreach (var segment in segments)
        {
            i++;
            if(j>=previousCheckpoint*3)
            {
            if (segment.IsPointInSegment(point))
            {
                return blockNumber;
            }
            }
            
            if (i == 3)//чекпоинт через каждые 3 сегмента
            {
                blockNumber++;
                i = 0;
            }

            j++;
        }

        return -1;//ошибка
    }
}

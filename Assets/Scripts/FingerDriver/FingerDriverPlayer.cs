using UnityEngine;
using UnityEngine.SceneManagement;

public class FingerDriverPlayer : MonoBehaviour
{
    [SerializeField] private FingerDriverTrack m_Track;
    [SerializeField] private FingerDriverInput m_Input;
    //точка по которой будет проверяться наъождение на трассе
    //вынесена в нос автомобиля
    [SerializeField] private Transform m_trackPoint;
    [SerializeField] private float m_CarSpeed = 2f;
    [SerializeField] private float m_MaxSteer = 90f;
    
    private int checkpointsCounter = 0;
    private int previousCheckpoint = 0;

    private void Update()
    {
        previousCheckpoint = m_Track.PreviousCheckPoint(m_trackPoint.position, previousCheckpoint);
        if (m_Track.IsPointInTrack(m_trackPoint.position, previousCheckpoint ))
        {
            transform.Translate(transform.up * Time.deltaTime * m_CarSpeed, Space.World);
            transform.Rotate(0f, 0f, m_MaxSteer * m_Input.SteerAxis * Time.deltaTime);
            
            if ((previousCheckpoint!=checkpointsCounter)&&(previousCheckpoint!=-1))
            {
                checkpointsCounter = previousCheckpoint;
                print($"Текущие очки: {checkpointsCounter}");
            }
        }
        else
        {
            print($"Финальный счёт: {checkpointsCounter}");
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
    }
}

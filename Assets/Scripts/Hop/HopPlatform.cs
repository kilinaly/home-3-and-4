using System;
using UnityEditor;
using UnityEngine;
using Random = UnityEngine.Random;


public class HopPlatform : MonoBehaviour
{
    [SerializeField] private GameObject m_BasePlatform;
    [SerializeField] private GameObject m_DonePlatform;
    [SerializeField] private GameObject m_SpeedPlatform;
    [SerializeField] private GameObject m_JumpOverOnePlatform;

    private int randomNumber;

    private void Start()
    {
        m_BasePlatform.SetActive(true);
        m_DonePlatform.SetActive(false);
        m_SpeedPlatform.SetActive(false);
        m_JumpOverOnePlatform.SetActive(false);
    }

    public void SetupDone()
    { 
        m_BasePlatform.SetActive(false);
        
        randomNumber = Random.Range(1,4);

        switch (randomNumber)
        {
            case 1 :
                m_JumpOverOnePlatform.SetActive(false);
                m_SpeedPlatform.SetActive(false);  
                m_DonePlatform.SetActive(true);
                HopPlayer.m_JumpDistance = 2f;
                HopPlayer.m_BallSpeed = 1f;
                HopPlayer.m_JumpHeight = 1f;
                break;
            case 2:
                m_JumpOverOnePlatform.SetActive(false);
                m_SpeedPlatform.SetActive(true); 
                m_DonePlatform.SetActive(false);
                HopPlayer.m_JumpDistance = 2f;
                HopPlayer.m_BallSpeed = 2f;
                HopPlayer.m_JumpHeight = 1.2f;
                break;
            case 3:
                m_JumpOverOnePlatform.SetActive(true);
                m_SpeedPlatform.SetActive(false);  
                m_DonePlatform.SetActive(false);
                HopPlayer.m_JumpDistance = 4f;
                HopPlayer.m_BallSpeed = 1f;
                HopPlayer.m_JumpHeight = 1f;
                break;
        }

    }
}
